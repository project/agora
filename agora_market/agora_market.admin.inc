<?php

//////////////////////////////////////////////////////////////////////////////
// Settings form

function agora_market_admin_settings() {
  $form = array();

  // TODO

  //$form['#submit'][] = 'agora_market_admin_settings_submit';
  return array_merge_recursive(system_settings_form($form), array('#theme' => 'agora_market_admin_settings', 'buttons' => array('#weight' => 99)));
}

function agora_market_admin_settings_validate($form, &$form_state) {
  extract($form_state['values'], EXTR_SKIP | EXTR_REFS);

  // TODO
}

function theme_agora_market_admin_settings($form) {
  return drupal_render($form);
}
