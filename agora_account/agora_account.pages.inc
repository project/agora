<?php

//////////////////////////////////////////////////////////////////////////////
// Accounts

function agora_account_user_page($owner = NULL) {
  global $user;
  drupal_set_title($title = ($user->uid == $owner->uid ? t('My accounts') : t("@name's accounts", array('@name' => $owner->name))));
  drupal_add_css(drupal_get_path('module', 'agora_account') . '/agora_account.css', 'module', 'all', FALSE);

  $account_types = agora_get_account_types();

  $head = array(t('Account name'), t('Account type'), t('Account #')/*, t('Description')*/);
  $rows = array();

  $result = db_query("SELECT n.nid FROM {node} n WHERE n.type = 'account' AND n.uid = %d ORDER BY n.title ASC", $owner->uid);
  while ($node = db_fetch_object($result)) {
    if (($node = node_load($node->nid)) && node_access('view', $node)) {
      $account_type = $account_types[$node->account['module']];

      $rows[] = array(
        l($node->title, 'node/' . $node->nid),
        empty($node->account['type']) ?
          $account_type->title :
          t('!module (!type)', array('!module' => $account_type->title, '!type' => $account_type->types[$node->account['type']])),
        empty($node->account['number']) ? '-' : check_plain($node->account['number']),
        //empty($node->teaser) ? '-' : strip_tags($node->teaser),
      );
    }
  }

  if (empty($rows)) {
    $rows[] = array(array('data' => t('No accounts configured.'), 'colspan' => '3'));
  }

  return theme('table', $head, $rows, array('class' => 'agora accounts'));
}
