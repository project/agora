
if (Drupal.jsEnabled) {
  $(document).ready(function() {

    // Code for the <node/add/account> screen:
    if ($("#edit-account-module").length > 0) {
      var accountTypes = Drupal.settings.agora.accountTypes;

      var setOptions = function(select, options) {
        jQuery.each(options, function(k, v) {
          option = document.createElement("option");
          option.value = k;
          option.text  = v;
          try { select.add(option, null); } catch(e) { select.add(option); /* IE only */ }
        });
      };

      var setAccountTypeOptions = function(module) {
        var select = $("#edit-account-type");
        if (!select.attr('disabled')) {
          select.html(''); // remove all options
          setOptions(select[0], accountTypes[module].types);
        }
      };

      var showAccountType = function(toggle) {
        $("#edit-account-type-wrapper").css("display", (toggle ? "block" : "none"));
      };

      var showAccountNumber = function(toggle) {
        $("#edit-account-number-wrapper").css("display", (toggle ? "block" : "none"));
      };

      var changeAccountModule = function(module) {
        setAccountTypeOptions(module);
        showAccountType(accountTypes[module].type);
        showAccountNumber(accountTypes[module].number);
      };

      changeAccountModule($("#edit-account-module").val());
      $("#edit-account-module").change(function() {
        changeAccountModule(this.value);
      });
    }
  });
}
