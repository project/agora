<?php

//////////////////////////////////////////////////////////////////////////////
// Settings form

function agora_account_admin_settings() {
  $form = array();

  $form['accounts'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Account types'),
    '#collapsible'   => TRUE,
    '#collapsed'     => FALSE,
    '#description'   => agora_account_help('admin/settings/agora/accounts#account-types'),
  );
  $form['accounts']['agora_account_types'] = array(
    '#type'          => 'checkboxes',
    '#title'         => '',
    '#options'       => array_combine(array_keys($account_types = agora_get_account_types('title')), array_fill(0, count($account_types), '')),
    '#default_value' => array_keys(agora_get_account_types(NULL, TRUE)),
    '#description'   => t(''),
  );

  //$form['#submit'][] = 'agora_account_admin_settings_submit';
  return array_merge_recursive(system_settings_form($form), array('#theme' => 'agora_account_admin_settings', 'buttons' => array('#weight' => 99)));
}

function agora_account_admin_settings_validate($form, &$form_state) {
  extract($form_state['values'], EXTR_SKIP | EXTR_REFS);

  // Filter out disabled account types:
  $agora_account_types = array_filter($agora_account_types, 'is_string');
}

function theme_agora_account_admin_settings($form) {
  $head = array(t('Enabled'), array('data' => t('Name'), 'width' => '100%'), t('Module')/*, array('data' => t('Operations'), 'colspan' => '2')*/);

  $rows = array();
  foreach (agora_get_account_types() as $account_type) {
    $module = unserialize(db_result(db_query("SELECT info FROM {system} WHERE type = 'module' AND name = '%s'", $account_type->module)));

    $rows[] = array(
      array('data' => drupal_render($form['accounts']['agora_account_types'][$account_type->module]), 'align' => 'center'),
      check_plain($account_type->title),
      str_replace(' ', '&nbsp;', !empty($module) ? $module['name'] : $account_type->module),
    );
  }

  if (empty($rows)) {
    $rows[] = array(array('data' => t('No account types available.'), 'colspan' => '3'));
  }

  $form['accounts']['#value'] = theme('table', $head, $rows, array('class' => 'agora accounts'));
  return drupal_render($form);
}
