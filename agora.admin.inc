<?php

//////////////////////////////////////////////////////////////////////////////
// Settings form

function agora_admin_settings() {
  $form = array();

  $form['currencies'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Currencies'),
    '#collapsible'   => TRUE,
    '#collapsed'     => FALSE,
    '#description'   => agora_help('admin/settings/agora#currencies'),
  );
  $form['currencies']['agora_currencies'] = array(
    '#type'          => 'checkboxes',
    '#title'         => '',
    '#options'       => array_combine(array_keys($currencies = agora_get_currencies('title')), array_fill(0, count($currencies), '')),
    '#default_value' => array_keys(agora_get_currencies(NULL, TRUE)),
    '#description'   => t(''),
  );

  //$form['#submit'][] = 'agora_admin_settings_submit';
  return array_merge_recursive(system_settings_form($form), array('#theme' => 'agora_admin_settings', 'buttons' => array('#weight' => 99)));
}

function agora_admin_settings_validate($form, &$form_state) {
  extract($form_state['values'], EXTR_SKIP | EXTR_REFS);

  // Filter out disabled currencies:
  $agora_currencies = array_filter($agora_currencies, 'is_string');
}

function theme_agora_admin_settings($form) {
  $head = array(t('Enabled'), t('Code'), array('data' => t('Name'), 'width' => '100%'), t('Module')/*, array('data' => t('Operations'), 'colspan' => '2')*/);

  $rows = array();
  foreach (agora_get_currencies() as $currency) {
    $module = unserialize(db_result(db_query("SELECT info FROM {system} WHERE type = 'module' AND name = '%s'", $currency->module)));

    $rows[] = array(
      array('data' => drupal_render($form['currencies']['agora_currencies'][$currency->code]), 'align' => 'center'),
      check_plain($currency->code),
      check_plain($currency->title),
      str_replace(' ', '&nbsp;', !empty($module) ? $module['name'] : $currency->module) . '&nbsp;',
    );
  }

  if (empty($rows)) {
    $rows[] = array(array('data' => t('No currencies available.'), 'colspan' => '3'));
  }

  $form['currencies']['#value'] = theme('table', $head, $rows, array('class' => 'agora currencies'));
  return drupal_render($form);
}
